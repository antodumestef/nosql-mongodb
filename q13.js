db.restaurant.find({
    $and:[
    {'cuisine': {$nin: ['American']}},
    {'grades.grade': {$in: ['A']}},
    {'borough': {$nin: ['Brooklyn']}}
    ]
    }).sort({'cuisine':1})