db.restaurant.updateMany(
{'address.zipcode': /^33/},
{ $push:
    { 'grades': 
        {$each: 
            [
            { date:new Date(), grade: 'A', score: '75'},
            { date:new Date(), grade: 'D', score: '12'}
            ]
        }
    }
})