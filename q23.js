db.restaurant.find({
    $and:[
    {'grades.grade': {$in: ['A']}},
    {'grades.score': {$eq: 9}},
    {'grades.date': {$eq: ISODate("2014-08-11T00:00:00Z")}}
]},
    {'name': true, 'grades': true})