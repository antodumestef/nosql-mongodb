db.restaurant.insertMany([
    { 
        address: { 
            building: '123', 
            coord: [44.8383,-0.568563], 
            street: '9 Rue Ausone', 
            zipcode: '33000'
        },
        borough: 'Saint-Pierre',
        cuisine: 'Orientale',
        grades: [
            {
                date: new Date(),
                grade: 'A',
                score: '100'
            },
            {
                date: new Date(),
                grade: 'B',
                score: '50'
            },
            {
                date: new Date(),
                grade: 'C',
                score: '25'
            }
        ],
        name: 'Mampuku',
        restaurant_id: '33000001'
    },
    { 
        address: { 
            building: '123', 
            coord: [44.840383,-0.5705943], 
            street: '15 Rue des Faussets', 
            zipcode: '33000'
        },
        borough: 'Bourse',
        cuisine: 'Italienne',
        grades: [
            {
                date: new Date(),
                grade: 'A',
                score: '100'
            },
            {
                date: new Date(),
                grade: 'B',
                score: '50'
            },
            {
                date: new Date(),
                grade: 'C',
                score: '25'
            }
        ],
        name: 'Osteria Pizzeria da Bartolo',
        restaurant_id: '33000002'
    },
    { 
        address: { 
            building: '123', 
            coord: [44.8547939,-0.5720442], 
            street: '71 Cours Portal', 
            zipcode: '33300'
        },
        borough: 'Chartrons',
        cuisine: 'Japonais',
        grades: [
            {
                date: new Date(),
                grade: 'A',
                score: '100'
            },
            {
                date: new Date(),
                grade: 'B',
                score: '50'
            },
            {
                date: new Date(),
                grade: 'C',
                score: '25'
            }
        ],
        name: 'Fufu cours Portal',
        restaurant_id: '33300001'
    },
    { 
        address: { 
            building: '123', 
            coord: [44.8325,-0.572901], 
            street: '262 Rue Sainte-Catherine', 
            zipcode: '33000'
        },
        borough: 'Victoire',
        cuisine: 'Thailandais',
        grades: [
            {
                date: new Date(),
                grade: 'A',
                score: '100'
            },
            {
                date: new Date(),
                grade: 'B',
                score: '50'
            },
            {
                date: new Date(),
                grade: 'C',
                score: '25'
            }
        ],
        name: 'Nobi Nobi',
        restaurant_id: '33000003'
    },
    { 
        address: { 
            building: '123', 
            coord: [44.8412,-0.573457], 
            street: '59 Rue Saint-Rémi', 
            zipcode: '33000'
        },
        borough: 'Bourse',
        cuisine: 'Sushi',
        grades: [
            {
                date: new Date(),
                grade: 'A',
                score: '100'
            },
            {
                date: new Date(),
                grade: 'B',
                score: '50'
            },
            {
                date: new Date(),
                grade: 'C',
                score: '25'
            }
        ],
        name: 'Sakura Sushi',
        restaurant_id: '33000004'
    },
])