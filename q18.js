db.restaurant.find({
    $or:[
    {'borough': {$nin: ['Bronx']}},
    {'borough': {$nin: ['Staten Island']}},
    {'borough': {$nin: ['Brooklyn']}}
]},
    {'name': true, 'borough': true, 'cuisine': true})